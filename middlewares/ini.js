
const Role = require("../models/role");

const createRoles = async () => {
    try {
      // Count Documents
      const count = await Role.estimatedDocumentCount();
      // check for existing roles
      if (count > 0) return;
      // Create default Roles
      const values = await Promise.all([
        new Role({ rol: "USER_ROLE" }).save(),
        new Role({ rol: "ADMIN_ROLE" }).save(),
      ]);
  
      console.log(values);
    } catch (error) {
      console.error(error);
    }
  };

module.exports = {
    createRoles,
}